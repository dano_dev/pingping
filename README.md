# PingPing
ping server for prometheus metric pulling

## Available environment variables
* `DANO_ENV`: environment of deployment, if not production, DEBUG will be True ( default: *production* ) 
* `PORT`: binding port number ( default: *5000* )
* `HOST`: binding host name ( default: *0.0.0.0* )
* `PING_PREFIX`: prefix of ping metrics ( default: *ping* )
* `METRICS_ENDPOINT`: prometheus metrics endpoint ( default: */metrics* )
