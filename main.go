package main

import (
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func getEnv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

// env settings
var danoEnv = getEnv("DANO_ENV", "production")

// variable settings
var metricsEndpoint = getEnv("METRICS_ENDPOINT", "/metrics")
var port = getEnv("PORT", "5000")
var host = getEnv("HOST", "0.0.0.0")
var pingPrefix = getEnv("PING_PREFIX", "ping")

// counter for pings
var pingCounter = promauto.NewCounterVec(prometheus.CounterOpts{
	Name: pingPrefix + "_total",
	Help: "The total number of ping by tags",
}, []string{"tag"})

// summary for request duration by path
var reqDur = promauto.NewSummaryVec(prometheus.SummaryOpts{
	Name: pingPrefix + "_request_duration_seconds",
	Help: "The HTTP request latencies in seconds.",
}, []string{"path"})

func setupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	r := gin.Default()

	// middleware to observe request duration
	r.Use(func(c *gin.Context) {
		path := c.Request.URL.String()
		if path == metricsEndpoint {
			c.Next()
			return
		}

		start := time.Now()
		c.Next()
		elapsed := float64(time.Since(start)) / float64(time.Second)

		path = "/" + strings.Split(path, "/")[1]
		reqDur.With(prometheus.Labels{"path": path}).Observe(elapsed)
	})

	// heartbeat
	r.GET("/heartbeat", func(c *gin.Context) {
		c.String(http.StatusOK, "doki doki")
	})

	// increase ping counter using tag
	r.GET("/ping/:tag", func(c *gin.Context) {
		tag := c.Params.ByName("tag")
		pingCounter.With(prometheus.Labels{"tag": tag}).Inc()
		c.String(http.StatusOK, "ok")
	})

	// Add prometheus endpoint
	r.GET(metricsEndpoint, gin.WrapH(promhttp.Handler()))

	return r
}

func main() {
	if danoEnv == "production" {
		gin.SetMode(gin.ReleaseMode)
	}

	r := setupRouter()
	err := r.Run(host + ":" + port)

	if err != nil {
		panic(err.Error())
	}
}
